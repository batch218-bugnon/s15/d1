console.log ("Hello world!");
// enclose with quotation marks string datas

console. log ("Hello world!");
// case sensitive (not space sensitive)

console.
log
(
	"Hello, world!"
)
// not line sensitive, can work without semi-colon

// ; delimeter
// we use delimeter to end our code (best practice)

// [COMMENTS]
// - single line comments use double slash
// Shortcut: CMD + /

// I am a single link comment 

/* Multi-line comment */
/* 
	I am 
	a 
	multi line comment
*/


// Syntax and statement

// Statements in programming are instructions that we tell to computer to perform (it's like grammar in English)
// Syntax in programming, it is the set of rules that describes how statements must be considered

// Variables 
/* 
	It is used to contain data 
	- syntaxt in declaring variables 
	- let/const variableName 
*/

let myVariable = "Hello";
	// assignment operator (=);
console.log(myVariable);
/* console.log(hello); // will result to not defined error

// 2 Types of Variable 
// const - constant variable (hindi nagbabago)
// let - nagbabago na variable


/* 
	Guide in writing variables:
		1. Use the 'let' keyword followed by variable name of your choosing and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For constat variables, use the 'const' keyword
		4. Variable names should be indivative or descriptive of the value being sotred. 
		5. Never name a variable starting with numbers
		6. Refrain from using space in declaring a variable

*/

// String
let productName = 'desktop computer';
console.log(productName);
// single quote will work

let product = "Alvin's computer";
console.log(product);

// Number
let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// Reassigning variable value
// Syntax
	// variableName = newValue;

productName = "Laptop";
console.log(productName);

// Jane will be the value of friend because that's the newest value of friend
let friend = "Kate";
friend = "Jane";
console.log(friend);

/* interest will not change because it's const

interest = 4.89;

*/

const pi = 3.14;
console.log(pi);

// Reassigning - a variable already have a value and we reassigning a new on
// Initialize - it is our first value

let supplier; // declaration
supplier ="John Smith Tradings"; //initializing
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrad = "Dell";
console.log(productCode,productBrad);

// Using a variable with a reserved keyword
/* 

const let = "hello";
console.log(let);

*/


// [SECTION] Data Types


// Strings 
// Strings are seties of characters that create a word, phrase, sentence, or anything related to creating a text.
// Strings in JS is enclosed with single ('') or (" ") double quotes
let country = 'Philippines';
let province = "Metro Manila";

let fullAddress = province + ', ' + country;
// we use + symbol to concatenate data / values

console.log(province + ', ' + country);
console.log(fullAddress);
console.log("Philippines" + ', ' + "Metro Manila");


// Escape Character (\)
// "\n" refers to creating a new line or set the text to next line;

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early.";
console.log(message);


message = "John\'s emplyee went home early";
console.log(message);




// Numbers


//  Integers / Whole number
let headcount = 26;
console.log(headcount);


// Decimal numbers / float
let grade = 98.7
console.log(grade);
// Js can store whole number or decimal point

//  Exponential notation / fraction
let planetDistance = 2e10
console.log(planetDistance);

console.log("John's grad last quarter is "  + grade);


// ARRAYS
// It store multiple values with similar data type

let grades = [98.8, 95.4, 90.2, 94.6];
	// arrayName // elements
console.log(grades);

// different data types
// storing diff data types inside an array is not recommended because it will not make sence in the context of programming.
let details = ["John", "Smith", 32, true];
console.log(details);

// OBJECTS 
// Objects are special kind of data type that is used to mimic real world objects/ items.
// Syntax 
	/*
		let/const objectName = {
			propertyA: value,
			propertyB: value,
		}
	*/


let person = {
	// fullName, age, isMarried, contact are called property/key
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
	/* address: {
		houseNumber: "345",
		city: "Manila",

	} */ 
}


console.log(person);

const myGrades = {
	firstGrading: 98.8,
	seconGrading: 95.4,
	thirdGrading: 90.2, 
	fourthGrading: 94.6
};

console.log(myGrades);


let stringValue = "abscd";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as objects

// 'type of' - 
// we use type of operator to retrieve / know the data type

console.log(typeof stringValue); //output: string
console.log(typeof numberValue); //output: number
console.log(typeof booleanValue); //output: boolean
console.log(typeof waterBills); //output: object
console.log(typeof myGrades); //output: object

// Constant Onjects and Arrays
// We cannot reassign the value of the variable, but we can change the elements of the constant arrays
const anime = ["Boruto", "One piece", "Code Geass", "Monster", "dan maci", "AOT", "Fairy Tale"];


//  index- is the position of the element starting zero 
// Boruto is 0, One Piece is 1 and so on
anime[0] = "Naruto";
console.log(anime);



//  NULL
// it is used to intentionally express the absence of a value 
let spouse = null;
console.log(spouse);


// UNDEFINED 
// represents the state of a variable that has been declared but without an assigned value
let fullName; // declaration
console.log(fullName);








